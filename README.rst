==============================
Configuration à la zc.buildout
==============================

``fd.conf`` supports configuration files with a syntax inspired by
``zc.buildout``, allowing ``${section:option}`` references in an
ini-style structure.


Release history
---------------

``fd.conf.FutureSyntaxError``, specialized from ``fd.conf.SyntaxError``,
is used for reserved syntax.


1.1.1 (2016-07-22)
~~~~~~~~~~~~~~~~~~

``fd.conf.getter.substituting``:  Don't return internal marker when no
default is provided; allow ``KeyError`` to be raised.

``fd.conf.KeyError``:  ``KeyError`` subclass that also derives from
``fd.conf.Error``.

Reserve use of the ``<`` option name for future implementation of
``zc.buildout`` macro syntax.


1.1.0 (2016-03-14)
~~~~~~~~~~~~~~~~~~

Reserve use of ``+=`` and ``-=`` as value separators for future
implementation of ``zc.buildout`` operators using that syntax.

Support collection of options from the command line.

Use ordered dicts to provide configuration data.

Always use specific exceptions that carry along source & lineno.


1.0.1 (2016-03-04)
~~~~~~~~~~~~~~~~~~

Fix resolution of substitution across section boundaries.


1.0.0 (2011-08-26)
~~~~~~~~~~~~~~~~~~

Initial release; not published to PyPI.
