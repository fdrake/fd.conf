"""\
Entry points for command-line tools.

"""

from __future__ import print_function

__docformat__ = "reStructuredText"

import fd.conf.getter
import fd.conf.layer
import optparse
import sys


def main(argv=None):
    if argv is None:
        argv = sys.argv
    p = optparse.OptionParser()
    p.add_option("-l", "--layer", action="store", default=None,
                 help=("section:option that should be used for layering;"
                       " this will be treated like buildout:extends"))
    options, args = p.parse_args(argv[1:])
    layer = None
    if options.layer:
        layer = options.layer.split(":", 1)
    data = fd.conf.layer.from_path(args[0], extends=layer)
    config = fd.conf.getter.substituting(data)
    print(config(args[1], args[2]).strip())
