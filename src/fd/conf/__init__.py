"""\
Top-level API for the fd.conf package.

"""

from six.moves import builtins


class Error(Exception):
    """Error from fd.conf package."""

    def __init__(self, message, *args, **kwargs):
        super(Error, self).__init__(message, *args)
        self.message = message


class OptionError(Error):
    """Something is wrong with an option specification."""

    def __init__(self, message, source, *args, **kwargs):
        super(OptionError, self).__init__(message, source, *args, **kwargs)
        self.source = source


class SyntaxError(Error):
    """Something is wrong with the syntax of a configuration file."""

    def __init__(self, message, source, lineno, *args, **kwargs):
        super(SyntaxError, self).__init__(
            message, source, lineno, *args, **kwargs)
        self.source = source
        self.lineno = lineno


class FutureSyntaxError(SyntaxError):
    """Encountered syntax reserved for future use."""


class KeyError(Error, builtins.KeyError):
    """KeyError that's also an fd.conf.Error."""

    def __init__(self, section, key):
        super(KeyError, self).__init__("no value for %s:%s" % (section, key))
        self.section = section
        self.key = key


class TypeError(Error, builtins.TypeError):
    """TypeError that's also an fd.conf.Error."""


class ValueError(Error, builtins.ValueError):
    """ValueError that's also an fd.conf.Error."""
