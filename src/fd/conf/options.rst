============================
Reading command line options
============================

Some applications (see ``zc.buildout`` for an example) want to accept
configuration settings from the command line, where each option has the
form

    section:key=value

or even

    key=value

with a default section name provided by the application.

This is supported by ``fd.conf`` as well.


Parsing from the command line only
----------------------------------

The ``from_options`` function can be used to parse option settings from
command line arguments.  The function accepts a list of options and
returns a configuration based on those settings:

    >>> from fd.conf.reader import from_options
    >>> from pprint import pprint

    >>> from_options([]) == {}
    True

    >>> cfg = from_options(['s:k=v', 'x:y=z'])
    >>> pprint(cfg)  # doctest: +NORMALIZE_WHITESPACE
    OrderedDict([('s', OrderedDict([('k', 'v')])),
                 ('x', OrderedDict([('y', 'z')]))])

A default section may be specified; options without a section
specification will be added to that section:

    >>> pprint(from_options(['s:k=v', 'y=z'], default_section='x'))
    ... # doctest: +NORMALIZE_WHITESPACE
    OrderedDict([('s', OrderedDict([('k', 'v')])),
                 ('x', OrderedDict([('y', 'z')]))])

If the default section to use is not specified, an option without a
section is disallowed:

    >>> from_options(['s:k=v', 'y=z'])
    Traceback (most recent call last):
    OptionError: ('option without section not allowed', None)

Empty section names and keys are not allowed:

    >>> from_options([':y=z'])
    Traceback (most recent call last):
    OptionError: ('the empty section name is not allowed', None)

    >>> from_options(['y:=z'])
    Traceback (most recent call last):
    OptionError: ('the empty key is not allowed', None)

Source identifiers are passed along with exceptions, though there is no
line number:

    >>> from_options([':y=z'], source='<command-line>')
    Traceback (most recent call last):
    OptionError: ('the empty section name is not allowed', '<command-line>')

    >>> from_options(['y:=z'], source='splat!')
    Traceback (most recent call last):
    OptionError: ('the empty key is not allowed', 'splat!')

    >>> cfg = from_options(['s:k=v', 'y=z'],
    ...                    default_section='x', source='<cmdline>')

    >>> cfg['s']['k'].lineno is None
    True
    >>> cfg['s']['k'].source
    '<cmdline>'


Combining from a command line and files
---------------------------------------

Loading configuration from the command line and a configuration file can
be accomplished using the layered configuration ``Context`` object.
Create a context, then use the ``from_options`` and ``from_file``
methods to create a combined configuration:

    >>> from fd.conf import layer

    >>> context = layer.Context(None, None)
    >>> context.from_options(['s:k=v', 'x:y=z'], source='<cmdline>')
    >>> context.from_file(wrapper('''
    ... [s]
    ... a = b
    ... k = 42
    ... [z]
    ... answer = 42
    ... question = Work in progress
    ... '''), source='somefile.conf')

    >>> pprint(context.config)  # doctest: +NORMALIZE_WHITESPACE
    OrderedDict([('s', OrderedDict([('k', 'v'), ('a', ' b\n')])),
                 ('x', OrderedDict([('y', 'z')])),
                 ('z', OrderedDict([('answer', ' 42\n'),
                                    ('question', ' Work in progress\n')]))])

    >>> context.config['s']['k'].source
    '<cmdline>'
    >>> context.config['z']['answer'].lineno
    6
    >>> context.config['z']['answer'].source
    'somefile.conf'

Note that the options from the command line take precedence over those
from the file.  This is exactly like the command specifying that it
extends the configuration found in the file, but does not require the
extension be spelled out.


Syntax reserved for future use
------------------------------

The ``+=`` and ``-=`` operators are reserved for future use.  This means
that if you need a key that ends in a ``+`` or ``-`` character, a space
is required between the key and the ``=`` operator.  We can find the
exception classes in ``fd.conf``:

    >>> import fd.conf

    >>> def from_options_error(*args, **kwargs):
    ...     try:
    ...         from_options(*args, **kwargs)
    ...     except fd.conf.SyntaxError as e:
    ...         return e
    ...     else:
    ...         raise AssertionError("expected SyntaxError")

    >>> e = from_options_error(["sect:key+=value"], source="<cmdline>")

    >>> e.message
    "value indicator '+=' reserved for future use"
    >>> e.source
    '<cmdline>'
    >>> e.lineno

    >>> from_options(["sect:key+ =value"], source="<cmdline>")
    OrderedDict([('sect', OrderedDict([('key+', 'value')]))])

    >>> e = from_options_error(
    ...     ["key+=value"], default_section='sect', source="<cmdline>")

    >>> e.message
    "value indicator '+=' reserved for future use"
    >>> e.source
    '<cmdline>'
    >>> e.lineno

    >>> from_options(["key+ =value"],
    ...              default_section='sect', source="<cmdline>")
    OrderedDict([('sect', OrderedDict([('key+', 'value')]))])

    >>> e = from_options_error(["sect:key-=value"], source="<cmdline>")

    >>> e.message
    "value indicator '-=' reserved for future use"
    >>> e.source
    '<cmdline>'
    >>> e.lineno

    >>> from_options(["sect:key- =value"], source="<some-file>")
    OrderedDict([('sect', OrderedDict([('key-', 'value')]))])

    >>> e = from_options_error(
    ...     ["key-=value"], default_section='sect', source="<cmdline>")

    >>> e.message
    "value indicator '-=' reserved for future use"
    >>> e.source
    '<cmdline>'
    >>> e.lineno

    >>> from_options(["key- =value"],
    ...              default_section='sect', source="<cmdline>")
    OrderedDict([('sect', OrderedDict([('key-', 'value')]))])

The option name ``<`` is also reserved, allowing for future
implementation of the correspondign ``zc.buildout`` "macro" syntax:

    >>> e = from_options_error(
    ...     ["<=value"], default_section='sect', source="<cmdline>")

    >>> e.message
    "option name '<' reserved for future use"
    >>> e.source
    '<cmdline>'
    >>> e.lineno

    >>> e = from_options_error(
    ...     ["< =value"], default_section='sect', source="<cmdline>")

    >>> e.message
    "option name '<' reserved for future use"
    >>> e.source
    '<cmdline>'
    >>> e.lineno
