=======================
Configuration accessors
=======================

Most applications access configuration values in some single way; the
accessors provided in the ``fd.conf.getter`` support a number of
useful ways to get configuration data.

Let's import the module:

    >>> from fd.conf import getter

We'll also need a parsed configuration:

    >>> import fd.conf.reader

    >>> cfg = fd.conf.reader.from_file(wrapper('''\
    ... [s1]
    ... one=1
    ... two=2
    ... [s2]
    ... three=
    ...     3
    ... 
    ... four= 4
    ...       5 '''), "<source-one>")


Access to annotated strings
---------------------------

Applications that want to get the values annotated with their source
information can use the ``raw`` accessor:

    >>> get = getter.raw(cfg)

Values can be retrieved by passing the section and key to the getter:

    >>> s = get("s1", "one")
    >>> s
    '1\n'
    >>> s.source
    '<source-one>'
    >>> s.lineno
    2

    >>> s = get("s2", "four")
    >>> s
    ' 4\n      5 '
    >>> s.source
    '<source-one>'
    >>> s.lineno
    8

If there's no matching section or key, ``KeyError`` is raised:

    >>> get("not-there", "one")
    Traceback (most recent call last):
    KeyError: 'no value for not-there:one'

    >>> get("s1", "not-there")
    Traceback (most recent call last):
    KeyError: 'no value for s1:not-there'

Accessors take an optional default value.  If there's a matching value
in the configuration, that'll still be returned:

    >>> s = get("s1", "one", "<default>")
    >>> s
    '1\n'
    >>> s.source
    '<source-one>'
    >>> s.lineno
    2

    >>> s = get("s2", "four", "<default>")
    >>> s
    ' 4\n      5 '
    >>> s.source
    '<source-one>'
    >>> s.lineno
    8

If there isn't a matching configuration value and the default is a
string, the default is returned wrapped in annotations in the same way
values provided by the configuration reader are provided:

    >>> s = get("not-there", "one", "<default>")
    >>> s
    '<default>'
    >>> s.source
    >>> s.lineno

    >>> s = get("s1", "not-there", "<default>")
    >>> s
    '<default>'
    >>> s.source
    >>> s.lineno

If the default passed in is already wrapped, the original wrapper is
passed back:

    >>> wv = fd.conf.reader.annotated("<default>", "<source>", 42)

    >>> get("not-there", "one", wv) is wv
    True
    >>> get("s1", "not-there", wv) is wv
    True


Access to stripped strings
--------------------------

Perhaps even more commonly, applications aren't interested in the
surrounding whitespace, and would find it more convenient for that to be
removed.  This is supported by the ``stripped`` accessor:

    >>> get = getter.stripped(cfg)

This accessor returns the value with the annotations stripped:

    >>> get("s1", "one")
    '1'
    >>> get("s2", "four")
    '4\n      5'

If there's no matching section or key, ``KeyError`` is raised:

    >>> get("not-there", "one")
    Traceback (most recent call last):
    KeyError: 'no value for not-there:one'

    >>> get("s1", "not-there")
    Traceback (most recent call last):
    KeyError: 'no value for s1:not-there'

When the optional default value is provided and there's a matching value
in the configuration, that'll still be returned stripped:

    >>> get("s1", "one", "<default>")
    '1'
    >>> get("s2", "four", "<default>")
    '4\n      5'

If there isn't a matching configuration value, the default is returned.
Defaults which contain leading or trailing whitespace are not themselves
stripped:

    >>> get("not-there", "one", " <default> \n ")
    ' <default> \n '
    >>> get("s1", "not-there", " <default> \n ")
    ' <default> \n '


Substitution syntax in strings
------------------------------

Frequently, more elaborate configurations need to reuse portions of
values in many places; it's best if those portions can be factored out.
The ``zc.buildout`` package uses a straight-forward syntax that uses a
key containing two keys separated by a comma, preceded by a dollar sign
and surrounded by a pair of curly braces:

    ${section:key}

This can then be embedded in values along with other textual content:

    /var/lib/myapp/${profile:name}/status.dat

When combined with the layered readers (see ``fd.conf.layer``), this
produces a powerful approach to creating parameterized configurations.

The substituting accessor takes a parsed configuration, just like the
other accessors.  Let's start with the configuration we have, to check
that the accessor works as expected when substitution isn't being used:

    >>> get = fd.conf.getter.substituting(cfg)
    >>> get("s1", "two")
    '2\n'
    >>> s = get("s2", "four")
    >>> s
    ' 4\n      5 '
    >>> s.source
    '<source-one>'
    >>> s.lineno
    8

Let's try another configuration, this time with substitutions:

    >>> cfg = fd.conf.reader.from_file(wrapper('''\
    ... [server]
    ... host = task.${server:domain}
    ... domain = example.net
    ... port = 80
    ... path = api/
    ... url = http://${server:host}:${server:port}/${server:path}
    ... '''), "<source-two>")

    >>> get = fd.conf.getter.substituting(cfg)

    >>> s = get("server", "url")
    >>> s
    ' http://task.example.net:80/api/\n'

Note that the content pulled into the inner references has been
stripped; this make composition of values a reasonable thing to achieve.

When the section name is left blank in references, the name of the
current section is used:

    >>> cfg = fd.conf.reader.from_file(wrapper('''\
    ... [server]
    ... host = task.${:domain}
    ... domain = example.net
    ... port = 80
    ... path = api/
    ... url = http://${:host}:${:port}/${:path}
    ... '''), "<source-three>")

    >>> get = fd.conf.getter.substituting(cfg)

    >>> s = get("server", "url")
    >>> s
    ' http://task.example.net:80/api/\n'

References to other sections work as well, and references in referenced
values are resolved recursively:

    >>> cfg = fd.conf.reader.from_file(wrapper('''\
    ... [domain]
    ... name = example.${:tld}
    ... tld = net
    ...
    ... [host]
    ... name = task.${domain:name}
    ...
    ... [server]
    ... port = 80
    ... path = api/
    ... url = http://${host:name}:${:port}/${:path}
    ... '''), "<source-four>")

    >>> get = fd.conf.getter.substituting(cfg)

    >>> s = get("server", "url")
    >>> s
    ' http://task.example.net:80/api/\n'

Recursive references are disallowed and trigger an exception:

    >>> cfg = fd.conf.reader.from_file(wrapper('''\
    ... [server]
    ... url = ${server:url}
    ... '''), "<source-five>")

    >>> get = fd.conf.getter.substituting(cfg)
    >>> get("server", "url")
    Traceback (most recent call last):
    ValueError: recursive reference to server:url
