"""\
Tools for accessing values from a parsed configuration.

"""
__docformat__ = "reStructuredText"

import re
import six

import fd.conf
import fd.conf.reader


_marker = object()


def raw(cfg):
    def get(section, key, default=_marker):
        value = _getraw(cfg, section, key, default)
        if (value is default) and not _isannotated(value):
            value = _annotated(value)
        return value
    return get


def stripped(cfg):
    def get(section, key, default=_marker):
        try:
            v = _getraw(cfg, section, key)
        except KeyError:
            if default is _marker:
                raise
            v = default
        else:
            s = v.strip()
            if v != s:
                v = _annotated(s, v.source, v.lineno)
        return v
    return get


def substituting(cfg):
    def get(section, key, default=_marker):
        try:
            raw = _getraw(cfg, section, key)
        except KeyError:
            if default is _marker:
                raise
            return default
        if "$" in raw:
            # substitution may be needed
            value = _substitute(raw, cfg, section, set([(section, key)]))
            if value is raw:
                pass
            elif value == raw:
                # recover the existing annotataions
                value = raw
            else:
                value = _annotated(value, raw.source, raw.lineno)
            return value
        return raw
    return get


# annotation helpers

def _isannotated(v):
    return isinstance(v, fd.conf.reader.ABase)


def _annotated(v, source=None, lineno=None):
    if not isinstance(v, fd.conf.reader.ABase):
        if isinstance(v, six.string_types):
            v = fd.conf.reader.annotated(v, source, lineno)
    return v


# lookup helper

def _getraw(cfg, section, key, default=_marker):
    value = cfg.get(section, {}).get(key, default)
    if value is _marker:
        raise fd.conf.KeyError(section, key)
    return value


# substitution workhorse

_next_ref = re.compile(r"\$[{$]").search
_parse_ref = re.compile(r"([^:}]*):([^:}]+)}").match


def _substitute(value, cfg, section, context):
    r = ""
    m = _next_ref(value)
    while m is not None:
        i = m.start()
        r += value[:i]
        sep = m.group()
        if sep == "$$":
            r += "$"
            value = value[i+2:]
            continue
        # need to look for a substitution spec
        m = _parse_ref(value, i+2)
        if m is None:
            e = fd.conf.SyntaxError(
                "invalid substitution syntax: %r" % (value,))
            e.bad_value = value
            raise e
        # found; perform substitution
        s, k = m.group(1, 2)
        s = s or section
        ref = s, k
        if ref in context:
            raise fd.conf.ValueError("recursive reference to %s:%s" % ref)
        v = _getraw(cfg, *ref).strip()
        context.add(ref)
        r += _substitute(v, cfg, s, context)
        context.remove(ref)
        value = value[m.end():]
        m = _next_ref(value)
    return r + value
