"""\
Support for reading layered configurations.

"""
__docformat__ = "reStructuredText"

import collections

import six

import fd.conf
import fd.conf.reader
import fd.conf.resource


def from_file(f, source=None, extends=None, resolver=None):
    if resolver is None:
        resolver = fd.conf.resource.URLResolver()
    if extends is not None:
        section, key = extends
        if not isinstance(section, six.string_types):
            raise fd.conf.TypeError("extends[0] must be a string")
        if not isinstance(key, six.string_types):
            raise fd.conf.TypeError("extends[1] must be a string")
    context = Context(extends, resolver)
    context.from_file(f, source)
    return context.config


def from_path(path, extends=None):
    f = open(path, "r")
    resolver = fd.conf.resource.PathResolver()
    return from_file(f, path, extends=extends, resolver=resolver)


class Context(object):

    extends_key = None
    extends_section = None

    def __init__(self, extends, resolver):
        if extends:
            self.extends_section, self.extends_key = extends
        self.already_read = set()
        self.config = collections.OrderedDict()
        self.resolver = resolver

    def from_options(self, options, default_section=None, source=None):
        if source is not None:
            if source in self.already_read:
                # This is quite a surprise; should probably scream.
                return
            self.already_read.add(source)
        cfg = fd.conf.reader.from_options(options, default_section, source)
        self.merge(cfg)

    def from_file(self, f, source=None):
        self.already_read.add(source)
        cfg = fd.conf.reader.from_file(f, source=source)
        self.merge(cfg)
        if self.extends_section in cfg:
            if self.extends_key in cfg[self.extends_section]:
                # There's something else to read; we need to compute
                # additional URLs based on source:
                v = cfg[self.extends_section][self.extends_key]
                resources = v.split()
                resources.reverse()
                for res in resources:
                    # Join & recursively load:
                    path = self.resolver.join(source, res)
                    if path in self.already_read:
                        continue
                    self.from_file(self.resolver.open(path), path)

    def merge(self, cfg):
        for section_name, mapping in cfg.items():
            if section_name not in self.config:
                self.config[section_name] = mapping
                continue
            section = self.config[section_name]
            for key, value in mapping.items():
                if key not in section:
                    section[key] = value


def open_url(url):
    raise NotImplementedError
