"""\
Resolver interfaces.

"""
__docformat__ = "reStructuredText"

from six.moves import urllib
import os.path


class PathResolver(object):

    def join(self, base, rel):
        return os.path.join(os.path.dirname(base), rel)

    def open(self, path):
        return open(path)


class URLResolver(object):

    def join(self, base, rel):
        return urllib.parse.urljoin(base, rel)

    def open(self, path):
        return urllib.request.urlopen(path)
