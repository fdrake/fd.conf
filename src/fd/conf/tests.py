"""\
Test harness for fd.conf.

"""

from __future__ import print_function

__docformat__ = "reStructuredText"

import doctest
import io
import fd.conf.layer
import unittest


class TestResolver(object):

    def __init__(self, resolver):
        self.content = {}
        self.ops = []
        self.resolver = resolver

    # Methods for the test to control what comes back:

    def display(self):
        for item in self.ops:
            item = list(item)
            print(item.pop(0), repr(item)[1:-1])
        self.ops = []

    def provide(self, path, content):
        self.content[path] = content

    def remove(self, path):
        if path in self.content:
            del self.content[path]

    # Resolver methods:

    def join(self, base, rel):
        self.ops.append(("join", base, rel))
        return self.resolver.join(base, rel)

    def open(self, path):
        self.ops.append(("open", path))
        if path in self.content:
            return wrapper(self.content[path])
        else:
            e = IOError(2, "No such file or directory")
            e.filename = path
            raise e


class Wrapper(object):
    __slots__ = "readline",


def wrapper(text):
    if isinstance(text, bytes):
        sio = io.BytesIO(text)
    else:
        sio = io.StringIO(text)
    w = Wrapper()
    w.readline = lambda: sio.readline()
    return w


def suite(path, **kw):
    globs = {
        "wrapper": wrapper,
        "TestResolver": TestResolver,
        }
    globs.update(kw)
    return doctest.DocFileSuite(path, globs=globs)


def test_suite():
    return unittest.TestSuite([
        #
        # These are roughly ordered from the simplest to the most
        # inclusive.
        #
        suite("iowrapper.rst"),
        suite("reader.rst"),
        suite("reader.rst", from_file=fd.conf.layer.from_file),
        suite("layer.rst"),
        suite("options.rst"),
        suite("getter.rst"),
        ])
