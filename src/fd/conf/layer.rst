===========================
Layered configuration files
===========================

.. This only discusses layered configuration; the reader.txt test is
   also used for the layered reader to make sure basic operation of the
   parser isn't compromised.

Configuration files can be used to extend other configuration files.

The ``fd.conf.layer`` module provides a concise API to read a
configuration that may extend base configurations.  The API ensures that
circular inclusions don't cause the infinite recursions, and that all
configurations are handled consistently.

Let's import the module:

    >>> import fd.conf.layer

There are parallel APIs to the ``fd.conf.reader`` API; these APIs take
some additional keyword arguments:

``extends``
    Tuple containing (*section*, *key*) that stores information about
    the base configurations.  There is no default.

``resolver``
    A resolver object that provides ``join`` and ``open`` operations.
    Some basic resolvers for file system paths and URLs are provided in
    ``fd.conf.resource``

We're going to use a test resolver that records what's asked of it and
provides only the data we specify here.

    >>> import fd.conf.resource
    >>> resolver = TestResolver(fd.conf.resource.URLResolver())

We can provide specific content for the resolver by calling the
``provide`` method:

    >>> resolver.provide(
    ...     "file:///home/user/sample.cfg",
    ...     '''\
    ... [section]
    ... key = value
    ... another=really, one more!
    ... ''')

    >>> resolver.open("file:///home/user/sample.cfg").readline()
    '[section]\n'

There's a ``display`` method that show (and re-sets) what's been shown
so far:

    >>> resolver.display()
    open 'file:///home/user/sample.cfg'

    >>> url = resolver.join("file:///home/user/sample.cfg", "not-there.cfg")
    >>> url
    'file:///home/user/not-there.cfg'
    >>> resolver.open(url) #doctest: +NORMALIZE_WHITESPACE
    Traceback (most recent call last):
    IOError: [Errno 2] No such file or directory:
             'file:///home/user/not-there.cfg'

    >>> resolver.display()
    join 'file:///home/user/sample.cfg', 'not-there.cfg'
    open 'file:///home/user/not-there.cfg'

Let's try an example that shows how to specify where to look for "base"
configurations:

    >>> cfg = fd.conf.layer.from_file(
    ...     wrapper('''
    ... [mysection]
    ... key = value
    ... '''),
    ...     "file:///home/user/top.cfg",
    ...     extends=("mysection", "extends"),
    ...     resolver=resolver)

    >>> s = cfg["mysection"]["key"]
    >>> s.lineno
    3
    >>> s.source
    'file:///home/user/top.cfg'
    >>> s
    ' value\n'

Note that no additional resources were loaded, and so contributed no
additional values:

    >>> resolver.display()

    >>> list(cfg)
    ['mysection']
    >>> list(cfg["mysection"])
    ['key']

This is because the configuration itself didn't refer to any other
configurations.  Let's use a configuration that's a little more
interesting, but loading it with the same arguments:

    >>> cfg = fd.conf.layer.from_file(
    ...     wrapper('''
    ... [mysection]
    ... extends = sample.cfg
    ... key = value
    ...
    ... [section]
    ... key = override
    ... '''),
    ...     "file:///home/user/top.cfg",
    ...     extends=("mysection", "extends"),
    ...     resolver=resolver)

The additional resource was loaded:

    >>> resolver.display()
    join 'file:///home/user/top.cfg', 'sample.cfg'
    open 'file:///home/user/sample.cfg'

We now have access to the sections and keys from the "base"
configuration:

    >>> sorted(cfg)
    ['mysection', 'section']
    >>> sorted(cfg["mysection"])
    ['extends', 'key']
    >>> sorted(cfg["section"])
    ['another', 'key']

Values from the outer configuration override values from base configurations:

    >>> s = cfg["section"]["key"]
    >>> s.lineno
    7
    >>> s.source
    'file:///home/user/top.cfg'
    >>> s
    ' override\n'

Where there's no value for a key in the outer configuration, values from
the base configurations are provided instead:

    >>> s = cfg["section"]["another"]
    >>> s.lineno
    3
    >>> s.source
    'file:///home/user/sample.cfg'
    >>> s
    'really, one more!\n'

Let's take a look at a case when a variable is provided by several base
layers at the leaves, and intermediate layers are used to knit the
layers together:

    >>> resolver.provide(
    ...     "file:///home/user/one.cfg",
    ...     "[part]\nkey=one")
    >>> resolver.provide(
    ...     "file:///home/user/two.cfg",
    ...     "[part]\nkey=two")
    >>> resolver.provide(
    ...     "file:///home/user/three.cfg",
    ...     "[part]\nkey=three")
    >>> resolver.provide(
    ...     "file:///home/user/four.cfg",
    ...     "[part]\nkey=four")

    >>> resolver.provide(
    ...     "file:///home/user/middle-one.cfg",
    ...     "[part]\nextends = one.cfg two.cfg")
    >>> resolver.provide(
    ...     "file:///home/user/middle-two.cfg",
    ...     "[part]\nextends = three.cfg four.cfg")

Let's get this loaded:

    >>> cfg = fd.conf.layer.from_file(
    ...     wrapper("[part]\nextends = middle-one.cfg middle-two.cfg"),
    ...     "file:///home/user/top.cfg",
    ...     extends=("part", "extends"),
    ...     resolver=resolver)

    >>> s = cfg["part"]["key"]
    >>> s.source
    'file:///home/user/four.cfg'
    >>> s
    'four'

    >>> resolver.display()
    join 'file:///home/user/top.cfg', 'middle-two.cfg'
    open 'file:///home/user/middle-two.cfg'
    join 'file:///home/user/middle-two.cfg', 'four.cfg'
    open 'file:///home/user/four.cfg'
    join 'file:///home/user/middle-two.cfg', 'three.cfg'
    open 'file:///home/user/three.cfg'
    join 'file:///home/user/top.cfg', 'middle-one.cfg'
    open 'file:///home/user/middle-one.cfg'
    join 'file:///home/user/middle-one.cfg', 'two.cfg'
    open 'file:///home/user/two.cfg'
    join 'file:///home/user/middle-one.cfg', 'one.cfg'
    open 'file:///home/user/one.cfg'
