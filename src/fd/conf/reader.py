"""\
Pseudo-INI file reader.

Simpler than ConfigParser, but supports beginning of line whitespace and
blank lines in values.

Probably needs a slightly more cooked API for real use.

This doesn't conform with the not-quite-a-spec at
http://www.cloanto.com/specs/ini.html, and isn't intended to.

"""
__docformat__ = "reStructuredText"

import collections
import re

import fd.conf


def from_path(path):
    f = open(path)
    try:
        return from_file(f, path)
    finally:
        f.close()


def from_file(f, source=None):
    if source is None:
        source = getattr(f, "name", None)

    lineno = 0
    sections = collections.OrderedDict()
    section = None
    key = None

    def error(message):
        raise fd.conf.SyntaxError(message, source, lineno)

    while True:
        line = f.readline()
        if not line:
            break
        lineno += 1

        c1 = line[:1]
        if c1 == "#":
            # comments and blank lines terminate the current value
            key = None
        elif c1 == "[":
            # section head
            s = line.strip()
            if not s.endswith("]"):
                error("improper section header")
            name = s[1:-1]
            if not name:
                error("the empty section name is not allowed")
            section = get_section(sections, name)
            key = None
        elif c1.isspace():
            # continuation line
            if key is None:
                if line.rstrip():
                    error("cannot continue outside value")
                continue
            prev = section[key]
            prev = annotated(prev + line, prev.source, prev.lineno)
            section[key] = prev
        elif "=" in line:
            # key = value (start)
            if section is None:
                error("cannot specify values outside sections")
            key, v = visplit(line, source, lineno)
            if not key:
                error("the empty key is not allowed")
            section[key] = annotated(v, source, lineno)
        else:
            # something non-white at the start of a line, without an "="
            error("unrecognized syntax")

    return sections


def from_options(options, default_section=None, source=None):
    config = collections.OrderedDict()
    for option in options:
        if ':' in option:
            sect, opt = option.split(':', 1)
            if not sect:
                raise fd.conf.OptionError(
                    "the empty section name is not allowed", source)
            if '=' in opt:
                opt, val = visplit(opt, source)
            else:
                exc = fd.conf.OptionError('value not specified', source)
                exc.section = sect or default_section
                exc.option = option
                raise exc
        elif '=' in option:
            sect = default_section
            opt, val = visplit(option, source)
        else:
            exc = fd.conf.OptionError(
                'bad option syntax (missing : and =)', source)
            exc.option = option
            raise exc
        sect = sect or default_section
        if not sect:
            raise fd.conf.OptionError(
                "option without section not allowed", source)
        if not opt:
            raise fd.conf.OptionError("the empty key is not allowed", source)
        section = get_section(config, sect)
        if opt in section:
            raise fd.conf.OptionError(
                "the same section:key cannot be specified by multiple options",
                source)
        section[opt] = annotated(val, source)
    return config


def get_section(sections, name):
    if name not in sections:
        sections[name] = collections.OrderedDict()
    return sections[name]


def visplit(s, source, lineno=None, _rx=re.compile(r'[-+]?=')):
    start, end = _rx.search(s).span()
    if s[start:end] in ('-=', '+='):
        raise fd.conf.FutureSyntaxError(
            "value indicator '%s' reserved for future use" % s[start:end],
            source, lineno)
    option = s[:start].rstrip()
    if option == '<':
        raise fd.conf.FutureSyntaxError(
            "option name '<' reserved for future use", source, lineno)
    return option, s[end:]


class ABase(object):

    __slots__ = ()

    def __init__(self, value, source=None, lineno=None):
        super(ABase, self).__init__()
        self.source = source
        self.lineno = lineno


class AStr(ABase, str):

    # Can't use __slots__ for str subclasses.

    def __new__(cls, value, source=None, lineno=None):
        return str.__new__(cls, value)


try:
    unicode
except NameError:

    def annotated(value, source, lineno=None):
        return AStr(value, source, lineno)

else:

    class AUnicode(ABase, unicode):

        __slots__ = "source", "lineno"

        def __new__(cls, value, source=None, lineno=None):
            return unicode.__new__(cls, value)

    def annotated(value, source, lineno=None):
        if isinstance(value, unicode):
            return AUnicode(value, source, lineno)
        else:
            return AStr(value, source, lineno)
