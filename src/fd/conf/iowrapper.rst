=======================
I/O wrapper for testing
=======================

The ``wrapper`` function provides an abbreviated but functional file
object from a text string:

    >>> f = wrapper("line 1\nline 2\n")
    >>> f.readline()
    'line 1\n'
    >>> f.name
    Traceback (most recent call last):
    AttributeError: 'Wrapper' object has no attribute 'name'

    >>> f.readline()
    'line 2\n'
    >>> f.readline()
    ''
    >>> f.readline()
    ''
