===========================
Reading configuration files
===========================

Configuration files are read using the ``fd.conf.reader`` module:

    >>> import fd.conf.reader

The configuration syntax is similar to the "ini" syntax accepted by
Python's ``ConfigParser`` module, but without the conflation of multiple
syntaxes evident in that module.  As with ``ConfigParser``, the
configuration syntax supported is only vaguely reminiscent of the
ill-defined "ini" syntax.

In all cases, the syntax reflects the most common practices of the
buildout configuration files used at Zope Corporation, and that only in
the strictest interpretation:

- "#" is used as the comment character.

- Comments must begin in the first column, and extend to the end of the
  line.

- Sections are marked by lines containing "[" in the first column and
  "]" in the last non-blank position.

- "=" is used as the key/value separator.

- All lines must fall into one of the following categories:

  - comment

  - section marker

  - key / value pair

  - value continuation

  - blank

The following additional qualities fall out from the implementation:

- Names may not include newline characters.

- Section names are not restricted in any way.

- Key names may not include leading or trailing whitespace, and may not
  include the "=" character.

- Section names may be used more than once; the resulting content of the
  section will be the last value given for each key.

- Key names may be given more than once; the last value encountered for
  the key will be used.


API
---

Configurations are read from files using the ``readline`` method; no
other methods or attributes are required.  The ``wrapper`` function
provided by the test harness sets this up for us.

The most basic function provided by the ``fd.conf.reader`` module is
``from_file``.  This function takes a file object and an optional source
label, and returns a dictionary of dictionaries.  The outer dictionary
maps section names to dictionaries representing the section contents.
The inner dictionary maps keys to objects providing the associated value
and information about where in the file the value was located.

Since this test is used for multiple from_file implementations, we're
only going to import the function if it hasn't been provided for us:

    >>> try:
    ...     from_file = from_file
    ... except NameError:
    ...     from fd.conf.reader import from_file

Let's look at this using a simple example for illustrative purposes:

    >>> f = wrapper("""\
    ... [section]
    ... 
    ... one = value 1
    ... 
    ... # This is a comment.
    ... 
    ... two=value 2
    ...     continuation
    ... # Another comment.
    ... 
    ... """)

    >>> cfg = from_file(f, "some/path.cfg")
    >>> isinstance(cfg, dict)
    True
    >>> list(cfg.keys())
    ['section']
    >>> section = cfg["section"]
    >>> isinstance(section, dict)
    True
    >>> sorted(section)
    ['one', 'two']

The values returned are objects that have some interesting attributes.
Note that trailing blank lines following the value are considered part
of the value, as are embedded newlines and leading whitespace on
continuation lines:

    >>> one = section["one"]
    >>> one
    ' value 1\n\n'
    >>> one.source
    'some/path.cfg'
    >>> one.lineno
    3

    >>> two = section["two"]
    >>> two
    'value 2\n    continuation\n'
    >>> two.source
    'some/path.cfg'
    >>> two.lineno
    7

Lines that start with whitespace followed by the "#" comment delimiter
are not comments, and can only be continuation lines:

    >>> cfg = from_file(wrapper("""\
    ... [section]
    ... key =
    ...     # part of the value
    ...     still value
    ...
    ...     # still more value
    ...
    ... """))

    >>> cfg["section"]["key"]
    '\n    # part of the value\n    still value\n\n    # still more value\n\n'

There are only a small number of error conditions.  First, any
non-blank, non-comment line before the first section marker is an error:

    >>> from_file(wrapper("not = legal\n"))
    Traceback (most recent call last):
    SyntaxError: ('cannot specify values outside sections', None, 1)

Lines that start with "[" must end with "]" as the last non-blank
character:

    >>> from_file(wrapper("[totally not ok \n"))
    Traceback (most recent call last):
    SyntaxError: ('improper section header', None, 1)

    >>> from_file(wrapper("[stil not ok] # comment??? \n"))
    Traceback (most recent call last):
    SyntaxError: ('improper section header', None, 1)

Non-blank lines with leading whitespace must be continuation lines
associated with a key, even if "#" is the first non-blank character:

    >>> from_file(wrapper("  not a continuation\n"))
    Traceback (most recent call last):
    SyntaxError: ('cannot continue outside value', None, 1)

    >>> from_file(wrapper("[section]\n  not a continuation\n"))
    Traceback (most recent call last):
    SyntaxError: ('cannot continue outside value', None, 2)

    >>> from_file(wrapper("""\
    ... [section]
    ... key = value
    ... # end of value
    ...     not a continuation
    ... """))
    Traceback (most recent call last):
    SyntaxError: ('cannot continue outside value', None, 4)

    >>> from_file(wrapper("  # not a comment\n"))
    Traceback (most recent call last):
    SyntaxError: ('cannot continue outside value', None, 1)

    >>> from_file(wrapper("[section]\n  # not a comment\n"))
    Traceback (most recent call last):
    SyntaxError: ('cannot continue outside value', None, 2)

    >>> from_file(wrapper("""\
    ... [section]
    ... key = value
    ... # end of value
    ...     # not a comment
    ... """))
    Traceback (most recent call last):
    SyntaxError: ('cannot continue outside value', None, 4)

Section names can include whitespace other than newlines:

    >>> cfg = from_file(wrapper("[ this is\ta\fsection ]\n"))
    >>> dict(cfg[" this is\ta\fsection "])
    {}

    >>> from_file(wrapper("[ this is not a section\n]\n"))
    Traceback (most recent call last):
    SyntaxError: ('improper section header', None, 1)

Section names cannot be empty:

    >>> from_file(wrapper("[]\n"))
    Traceback (most recent call last):
    SyntaxError: ('the empty section name is not allowed', None, 1)

Keys cannot be empty:

    >>> from_file(wrapper("[sect]\n= missing key\n"))
    Traceback (most recent call last):
    SyntaxError: ('the empty key is not allowed', None, 2)

Anything unrecognized is an error as well:

    >>> from_file(wrapper("not-a-key\n"))
    Traceback (most recent call last):
    SyntaxError: ('unrecognized syntax', None, 1)

    >>> from_file(wrapper("[section]\nnot-a-key\n"))
    Traceback (most recent call last):
    SyntaxError: ('unrecognized syntax', None, 2)


Syntax reserved for future use
------------------------------

The ``+=`` and ``-=`` operators are reserved for future use.  This means
that if you need a key that ends in a ``+`` or ``-`` character, a space
is required between the key and the ``=`` operator.  We can find the
exception classes in ``fd.conf``:

    >>> import fd.conf

    >>> try:
    ...     from_file(wrapper("[sect]\nkey+=value\n"), source="<some-file>")
    ... except fd.conf.FutureSyntaxError as e:
    ...     pass
    ... else:
    ...     print 'expected FutureSyntaxError'

    >>> e.message
    "value indicator '+=' reserved for future use"
    >>> e.source
    '<some-file>'
    >>> e.lineno
    2

    >>> from_file(wrapper("[sect]\nkey+ =value\n"), source="<some-file>")
    OrderedDict([('sect', OrderedDict([('key+', 'value\n')]))])

    >>> try:
    ...     from_file(wrapper("[sect]\nkey-=value\n"), source="<some-file>")
    ... except fd.conf.FutureSyntaxError as e:
    ...     pass
    ... else:
    ...     print 'expected FutureSyntaxError'

    >>> e.message
    "value indicator '-=' reserved for future use"
    >>> e.source
    '<some-file>'
    >>> e.lineno
    2

    >>> from_file(wrapper("[sect]\nkey- =value\n"), source="<some-file>")
    OrderedDict([('sect', OrderedDict([('key-', 'value\n')]))])

The option name ``<`` is also reserved, allowing for future
implementation of the corresponding ``zc.buildout`` "macro" syntax:

    >>> try:
    ...     from_file(wrapper("[sect]\n<=value\n"), source="<some-file>")
    ... except fd.conf.FutureSyntaxError as e:
    ...     pass
    ... else:
    ...     print 'expected FutureSyntaxError'

    >>> e.message
    "option name '<' reserved for future use"
    >>> e.source
    '<some-file>'
    >>> e.lineno
    2

    >>> try:
    ...     from_file(wrapper("[sect]\n< = value\n"), source="<some-file>")
    ... except fd.conf.FutureSyntaxError as e:
    ...     pass
    ... else:
    ...     print 'expected FutureSyntaxError'

    >>> e.message
    "option name '<' reserved for future use"
    >>> e.source
    '<some-file>'
    >>> e.lineno
    2
