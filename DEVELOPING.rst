=================
Development notes
=================


Release procedure
-----------------

#. Update release notes and add release date in README.rst.  Commit.

#. Set version in setup.py.  Commit.  Tag.

#. Build release artifacts.

#. Roll back the branch head to just before setting the version in
   setup.py:

       $ git reset --hard HEAD~1

#. Push changes and tag:

       $ git push && git push origin <tag>
