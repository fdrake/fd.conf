import setuptools


entry_points = '''\
[console_scripts]
fdconf = fd.conf.tool:main
'''


setuptools.setup(
    name="fd.conf",
    version="0",
    url="https://bitbucket.org/fdrake/fd.conf",
    author="Fred L. Drake, Jr.",
    author_email="fred@fdrake.net",
    classifiers=[
        "Intended Audience :: Developers",
        "License :: OSI Approved :: Zope Public License",
        "Programming Language :: Python :: 2.7",
    ],
    package_dir={"": "src"},
    packages=["fd", "fd.conf"],
    install_requires=[
        "setuptools",
        "six",
        "zope.testing",
        ],
    package_data={"fd.conf": ["*.txt"]},
    entry_points=entry_points,
    # Really should check this, but be safe for now.
    zip_safe=False,
    )
